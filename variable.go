package main

import (
	"fmt"
	"regexp"
)

func getAllowedArgumetnVariables() []string {
	return []string{"infile"}
}

func getVariables(input string) []string {
	anyVarRegexp := regexp.MustCompile(`\$\{([a-zA-Z0-9]+)\}`)
	matches := anyVarRegexp.FindAllStringSubmatch(input, -1)
	variables := []string{}
	for _, match := range matches {
		found := false
		for _, variable := range variables {
			if variable == match[1] {
				found = true
			}
		}
		if !found {
			variables = append(variables, match[1])
		}
	}
	return variables
}

func substituteVariables(inArguments []string, vars map[string]string) ([]string, error) {
	outArguments := []string{}
	for _, inArgument := range inArguments {
		tmpArgument := inArgument
		for variableName, variableValue := range vars {
			regex, err := regexp.Compile(fmt.Sprintf(`\$\{%s\}`, variableName))
			if err != nil {
				return nil, err
			}
			tmpArgument = regex.ReplaceAllString(tmpArgument, variableValue)
		}
		if variables := getVariables(tmpArgument); len(variables) > 0 {
			return nil, fmt.Errorf("Unknown Variable(s) %v in argument", variables)
		}
		outArguments = append(outArguments, tmpArgument)
	}
	return outArguments, nil
}
