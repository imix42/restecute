package main

import "testing"

func TestGetVariables(t *testing.T) {
	testData := []struct {
		Input     string
		Variables []string
	}{
		{"", []string{}},
		{"${infile}", []string{"infile"}},
		{"${infile} ${infile}", []string{"infile"}},
		{"${infile} ${outfile}${infile}", []string{"infile", "outfile"}},
	}
	for testIdx, td := range testData {
		vars := getVariables(td.Input)
		if len(vars) != len(td.Variables) {
			t.Fatal("Number of variables not equal")
		}
		for varIdx, varVal := range td.Variables {
			if varVal != vars[varIdx] {
				t.Fatalf("Test Nr: %d; Variavle not equal %d", testIdx, varIdx)
			}
		}
	}
}

func TestSubstituteVariables(t *testing.T) {
	testData := []struct {
		InArguments  []string
		Variables    map[string]string
		OutArguments []string
	}{
		{
			[]string{"${infile}"},
			map[string]string{"infile": "temppath"},
			[]string{"temppath"},
		},
		{
			[]string{"${infile} ${outfile}"},
			map[string]string{"infile": "temppath", "outfile": "another"},
			[]string{"temppath another"},
		},
	}
	for _, td := range testData {
		gotArguments, err := substituteVariables(td.InArguments, td.Variables)
		if err != nil {
			t.Fatal(err)
		}
		if len(gotArguments) != len(td.OutArguments) {
			t.Fatal("Argument lengths did not match.")
		}
		for idx, outArgument := range td.OutArguments {
			if gotArguments[idx] != outArgument {
				t.Fatalf("Arguments not match idx: %d", idx)
			}
		}
	}
}

func TestSubstituteVariablesUnknown(t *testing.T) {
	inArguments := []string{"${infile} ${unknown}"}
	variables := map[string]string{"infile": "temppath", "outfile": "another"}
	_, err := substituteVariables(inArguments, variables)
	if err.Error() != "Unknown Variable(s) [unknown] in argument" {
		t.Fatal(err)
	}
}
