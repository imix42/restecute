package main

import (
	"testing"

	"github.com/go-test/deep"
)

func TestGetHostString(t *testing.T) {
	testData := []struct {
		Config     *configuration
		HostString string
	}{
		{&configuration{"", 8080, nil}, ":8080"},
		{&configuration{"host", 80, nil}, "host:80"},
	}
	for _, td := range testData {
		if td.Config.getHostString() != td.HostString {
			t.Fatal("Host strings not equal")
		}
	}
}

type TestConfigFile struct {
	Path           string
	ExpectedConfig *configuration
}

func getTestConfigFiles() []TestConfigFile {
	minimalConfig := TestConfigFile{"./test/minimal.conf", &configuration{"", 8080, nil}}
	simpleEndpoint := endPoint{"/hello/world", "echo", []string{"hello world"}, ""}
	oneSimpleEndpoint := TestConfigFile{"./test/oneSimpleEndpoint.conf",
		&configuration{"", 8080, []endPoint{simpleEndpoint}}}
	testConfigFiles := []TestConfigFile{
		minimalConfig,
		oneSimpleEndpoint,
	}
	return testConfigFiles
}

func TestReadConfig(t *testing.T) {
	configFiles := getTestConfigFiles()
	for _, configFile := range configFiles {
		configRead, err := readConfig(configFile.Path)
		if err != nil {
			t.Fatal(err)
		}
		if diff := deep.Equal(configRead, configFile.ExpectedConfig); diff != nil {
			t.Error(diff)
		}
	}
}

type VerifyConfig struct {
	Config *configuration
	Error  verifyError
}

func getVerifyConfigs() []VerifyConfig {
	unknownCommand := configuration{"", 8080, []endPoint{{"/paht", "nonexist", []string{}, ""}}}
	verifyConfigs := []VerifyConfig{
		{&unknownCommand, verifyError{verifyCommandNotExisting, ""}},
	}
	return verifyConfigs
}

func TestVerfiyConfig(t *testing.T) {
	verifyConfigs := getVerifyConfigs()
	for _, verifyConfig := range verifyConfigs {
		err := verifyConfiguration(verifyConfig.Config)
		if err == nil {
			t.Fatal("No error found")
		}
		verifyErr, ok := err.(*verifyError)
		if !ok {
			t.Fatalf("Wrong error type: %v", err)
		}
		if verifyErr.Status != verifyConfig.Error.Status {
			t.Fatalf("Err Status not equal: %v, %v", verifyErr.Status, verifyConfig.Error.Status)
		}
	}
}

func TestVerifyArgumentVariables(t *testing.T) {
	testData := []struct {
		Arguments []string
		Err       *verifyError
	}{
		{[]string{"random argument"}, nil},
		{[]string{"${unknown}"}, &verifyError{verifyArgumentVariable, "Unknown variable: unknown"}},
		{[]string{"${infile}"}, nil},
	}
	for _, td := range testData {
		err := verifyArgumentVariables(td.Arguments)
		if err != nil {
			if td.Err != nil {
				verifyError, ok := err.(*verifyError)
				if !ok {
					t.Fatalf("Wrong type of error received %v", err)
				}
				if verifyError.Status != td.Err.Status {
					t.Fatal("Wrong status")
				}
				if verifyError.Message != td.Err.Message {
					t.Fatalf(`Wrong Message got "%s", want "%s"`, verifyError.Message, td.Err.Message)
				}
			} else {
				t.Fatal("err not nil, td.Err nil")
			}
		}
	}
}
