# RESTecute
[![Build Status](https://gitlab.com/imix42/restecute/badges/master/build.svg)](https://gitlab.com/imix42/restecute/commits/master) [![Coverage Report](https://gitlab.com/imix42/restecute/badges/master/coverage.svg)](https://gitlab.com/imix42/restecute/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/imix42/restecute)](https://goreportcard.com/report/gitlab.com/imix42/restecute) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

## Introduction
RESTecute is a small tool that allows the ececution of commands with a REST
API.

## Status
This project is currently highly experimental and not fit in any way for
production use.

## Usage
Restecute is configured with a config file which uses [YAML](https://en.wikipedia.org/wiki/YAML).

You can use variables in the config file. There are some variables which are
always available. Currently the following variables are available:

| variable | description |
| -------- | -------- |
| infile | Path to a file which contains the data in the body of the request. |
